//  Selectors :-) 

const todoUserInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoContainer = document.querySelector('.todo-container');


// EventListners
todoButton.addEventListener('click', (e) => {

    if (!todoUserInput.value) {
        alert('Please fill out the task before adding.');
        return;
    }

    addTask(todoUserInput.value);

});

// Functions

function addTask(value) {

    const newDiv = document.createElement('div');
    newDiv.classList.add("todo");

    const todoCheck = document.createElement('input');
    todoCheck.classList.add("check-box");
    todoCheck.type = "checkbox";
    newDiv.appendChild(todoCheck);

    const todoInput = document.createElement('input');
    todoInput.classList.add("input-field");
    todoInput.type = "text";
    todoInput.value = value;
    todoInput.disabled = true;
    newDiv.appendChild(todoInput);


    const editButton = document.createElement('button');
    editButton.classList.add("edit-btn");
    editButton.innerHTML = "Edit";
    newDiv.appendChild(editButton);


    const deleteButton = document.createElement('button');
    deleteButton.classList.add("delete-btn");
    deleteButton.innerHTML = "Delete";
    newDiv.appendChild(deleteButton);

    todoContainer.appendChild(newDiv);

    // edit Button function
    let index = -1;
    editButton.addEventListener('click', (e) => {
        let filter = todoInput.value;
        let dataArr = JSON.parse(localStorage.getItem('todos'))

        let tempIndex = dataArr.indexOf(filter);
        if (tempIndex >= 0) {
            index = tempIndex;
        }


        if (editButton.innerText.toLowerCase() == "edit") {
            editButton.innerText = "Save";

            editButton.style.background = "green";
        } else {
            editButton.innerText = "Edit";

            editButton.style.background = "grey";


            if (index >= 0) {
                dataArr[index] = todoInput.value

                localStorage['todos'] = JSON.stringify(dataArr)

            }

        }
        todoInput.disabled = !todoInput.disabled;
    });

    // delete button function
    deleteButton.addEventListener('click', (e) => {
        todoContainer.removeChild(newDiv);
        removeLocalTodos(e.target.parentElement);
    });

    saveLocalTodos(todoUserInput.value);

    todoUserInput.value = "";
}

// Save LocalStorage todo

function saveLocalTodos(todo) {
    let todos;
    if (!localStorage['todos']) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }
    todos.push(todo);
    localStorage.setItem('todos', JSON.stringify(todos));
}

// Remove todo items


function removeLocalTodos(todo) {
    let todos;
    if (JSON.parse(localStorage['todos']).length == 0) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }
    const todoIndex = todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.setItem('todos', JSON.stringify(todos));
}


// get Todos
function getTodos() {
    let todos;
    if (JSON.parse(localStorage.getItem('todos')).length == 0) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }

    todos.forEach(function(todo) {

        const newDiv = document.createElement('div');
        newDiv.classList.add("todo");

        const todoCheck = document.createElement('input');
        todoCheck.classList.add("check-box");
        todoCheck.type = "checkbox";
        newDiv.appendChild(todoCheck);

        const todoInput = document.createElement('input');
        todoInput.classList.add("input-field");
        todoInput.type = "text";
        todoInput.value = todo;
        todoInput.disabled = true;
        newDiv.appendChild(todoInput);


        const editButton = document.createElement('button');
        editButton.classList.add("edit-btn");
        editButton.innerHTML = "Edit";
        newDiv.appendChild(editButton);


        const deleteButton = document.createElement('button');
        deleteButton.classList.add("delete-btn");
        deleteButton.innerHTML = "Delete";
        newDiv.appendChild(deleteButton);

        todoContainer.appendChild(newDiv);

        // edit Button function
        let index = -1
        editButton.addEventListener('click', (e) => {

            let dataArr = JSON.parse(localStorage.getItem('todos'))


            let filter = todoInput.value;

            let tempIndex = dataArr.indexOf(filter);
            // console.log(index)

            if (tempIndex >= 0) {
                index = tempIndex;
            }

            if (editButton.innerText.toLowerCase() == "edit") {
                editButton.innerText = "Save";
                editButton.style.background = "green";
            } else {


                // console.log(filter)
                // console.log(dataArr)
                editButton.innerText = "Edit";
                editButton.style.background = "grey";
                if (index >= 0) {
                    dataArr[index] = todoInput.value

                    localStorage['todos'] = JSON.stringify(dataArr)

                }

            }
            todoInput.disabled = !todoInput.disabled;
        });

        // delete button function
        deleteButton.addEventListener('click', (e) => {
            todoContainer.removeChild(newDiv);
            removeLocalTodos(e.target.parentElement);
        });
    });
}


getTodos()